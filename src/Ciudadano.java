import java.util.*;

public class Ciudadano {
	
	private int id;
	
	private ArrayList comunidad;
	
	private boolean enfermedad;
	
	private String estado;
	
	private int edad;
	
	private EnfermedadBase enfermedadBase;
	
	private Afecciones afecciones;
	
	private double Suma=0;

	private int conexionesFisicasConOtros; 
	
	private int Steps=0;
	
	private String vacuna="";
	
	Ciudadano(){
		
	}
	
	
	public void setAfecc(Afecciones Afec) {
		this.afecciones = Afec;
		afecciones.generarAfeccion();
	}
	
	public void setEnfeBase(EnfermedadBase EnfeBase) {
		this.enfermedadBase = EnfeBase;
		enfermedadBase.generarEnfermedadBase();
	}
	
	public void unPasoMas() {
		this.Steps = Steps++; 
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ArrayList getComunidad() {
		return comunidad;
	}

	public void setComunidad(ArrayList comunidad) {
		this.comunidad = comunidad;
	}

	public boolean getEnfermedad() {
		return enfermedad;
	}

	public void setEnfermedad(boolean b) {
		this.enfermedad = b;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad() {
		Random nEdad = new Random(); 
		int nedad = nEdad.nextInt(120); 
		this.edad = nedad;
	}

	public int getConexionesFisicasConOtros() {
		return conexionesFisicasConOtros;
	}

	public void setConexionesFisicasConOtros(int conexionesFisicasConOtros) {
		this.conexionesFisicasConOtros = conexionesFisicasConOtros;
	}
	
	public double getGravedad() {
		if (afecciones.isTieneAfec()) {
			Suma= Suma + afecciones.generarGravedad();
		}
		if (enfermedadBase.isTieneEnfeB()) {
			Suma= Suma + enfermedadBase.generarGravedad();
		}
		return Suma;
	}


	public int getSteps() {
		return Steps;
	}


	public String getVacuna() {
		return vacuna;
	}


	public void setVacuna(String vacuna) {
		this.vacuna = vacuna;
	}
	
}
