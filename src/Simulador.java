import java.util.*;
public class Simulador {
	
	private int pasos;
	
	private Comunidad com;
	
	private Enfermedad ml;
	
	Simulador(){
		
		
	}
	
	public void añadirComunidad(Comunidad com) {
		this.com = com;
	}
	
	public void añadirEnfermedad(Enfermedad ml) {
		this.ml = ml;
	}
	
	public void simulacionDePasos() {
		
		for (int i=0; i<com.getNumInfectados();i++) {
			Random  ElInfectado = new Random();
			int ElInfect = ElInfectado.nextInt(100)+1;
			com.getlista().get(ElInfect).setEnfermedad(true);
		}
		
		
		ArrayList <HashSet> listaJuntas = new ArrayList();
		for (int i=0; i<pasos; i++) { //Simulacion de los pasos
			
			HashSet junta = new HashSet();
			
			Random tamJunta = new Random();
			int TJunta = tamJunta.nextInt(10)+2;
			
			Vacunas v = new Vacunas(com.getNumCiudadanos());
			
			while (junta.size()!=TJunta) {
				Random persoJunta = new Random();
				int PJunta = persoJunta.nextInt(com.getNumCiudadanos());
				if (com.getlista().get(PJunta).getConexionesFisicasConOtros()>0) {
					junta.add(PJunta);
				
				}
				if (junta.size()==TJunta) {
					listaJuntas.add(junta);
				}
			}
			Iterator Juntas = junta.iterator();
			Iterator Juntas2 = junta.iterator();
			for (int e=0; e<com.getNumCiudadanos();e++) {
				while (Juntas.hasNext()) {
				int num = (int)Juntas.next();
					if (com.getlista().get(num).getEnfermedad()) {
						Random ProbaConFis = new Random();
						int PCF = ProbaConFis.nextInt(100)+1;
						if (PCF>com.getProbabilidadConexionFisica()*100) {//Si la coneccion es fisica o no 
							while(Juntas2.hasNext()) {//Se vuelve a recorrer el HashSet para calcular si se enferma o no
								com.getlista().get((int)Juntas2.next()).setEnfermedad(ml.infeccion());//Se le aplica un valor verdadero si en ml.infeccion() si es verdadero
							}
						}
					}
				
				}
			}
			
			for (int e=0; e<com.getNumCiudadanos(); e++) {//Se recorre la lista con los ciudadanos
				if (com.getlista().get(e).getEnfermedad()) {//Se busca quien esta contagiado con la enfermedad
					com.getlista().get(e).setEstado(ml.calculoDeGravedad(com.getlista().get(e).getGravedad(), com.getlista().get(e).getEdad(), com.getlista().get(e).getSteps(), com.getlista().get(e).getVacuna()));//Se le asigna una gravedad dependiendo de su estado anterior (enfermedad base y/o afecciones)y edad
					com.getlista().get(e).setVacuna(v.AsignacionDeVacunas());
					com.getlista().get(e).unPasoMas();
					
				}
			}
			
			
			
		}
		for (int i=0; i<listaJuntas.size(); i++) {
			System.out.println(listaJuntas.get(i) + "  " +i);
		}
	}

	public int getPasos() {
		return pasos;
	}

	public void setPasos(int pasos) {
		this.pasos = pasos;
	}
	
	
	
}
