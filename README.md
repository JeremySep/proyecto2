Proyecto 2 Programacion Avanzada.

Requiere de una version de Java 11 o superior.

El presente proyecto esta constituido por las clases Main, Simulador, Enfermedad, Ciudadano, Comunidad, Afecciones, EnfermedadBase y Vacunas.
Tiene por objetivo simular el comportamiento de una Enfermedad, cualesquiera sea esta,
que se desarrolla en una Comunidad la cual posee individuos llamados Ciudadanos; Algunos
de estos ciudadanos portan la enfermedad siendo estos llamados infectados. Ademas de esto, los ciudadanos no son
iguales unos con otros, ya que pueden padecer afecciones (obesidad o desnutricion) y Enfermedades de Base, las cuales
al padecerlas, el individuo se vera mas afectado por la enfermedad infecciosa.

En la clase Main se definieron los valores para las variables Enfermedad, Comunidad y Simulador:

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Enfermedad ml = new Enfermedad();
		ml.setInfeccionProbable(0.3);
		ml.setPromedioPasos(18);
		
		Comunidad com = new Comunidad();
		com.setNumCiudadanos(2000);
		com.setPromedioConexionFisica(8);
		com.setEnfermedad("malaria");
		com.setNumInfectados(10);
		com.setProbabilidadConexionFisica(0.9);
		com.creaComunidad();
		
		Simulador s = new Simulador();
		s.setPasos(45);
		s.añadirComunidad(com);
		s.añadirEnfermedad(ml);
		s.simulacionDePasos();
		
	}

}
Cada clase se instancia y se le agregan sus valores respectivos, desde la linea 19 a 21 se genera la clase Enfermedad con sus valores,
desde la linea 23 hasta la 29 se crea Comunidad junto con sus valores, y por ultimo desde la linea 31 a la 35 se genera la clase Simulador con sus valores respectivos.
